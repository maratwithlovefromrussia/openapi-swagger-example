package psu.repository;

import psu.models.Payment;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class PaymentRepository {
    private static PaymentRepository instance;
    List<Payment> paymentDataBase = new LinkedList<>();

    private PaymentRepository() {
    }

    public static PaymentRepository getInstance() {
        if (instance == null) {
            instance = new PaymentRepository();
        }
        return instance;
    }

    public void create(Payment newPayment) {
        paymentDataBase.add(newPayment);
    }

    public Payment get(Integer id) {
        if (paymentDataBase.isEmpty()) {
            return new Payment();
        }

        return paymentDataBase.stream().filter(payment -> payment.getUserId() == id).collect(Collectors.toList()).get(0);
    }

    public void delete(Integer id) {
        if (paymentDataBase.isEmpty()) {
            return;
        }
        var payment1 = paymentDataBase.stream().filter(payment -> payment.getUserId() == id).collect(Collectors.toList()).get(0);
        paymentDataBase.remove(payment1);
    }

}
