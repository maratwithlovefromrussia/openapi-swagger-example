package psu.models;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "Payment", description = "Платеж")
public class Payment {
    @Schema(description = "Иденитификатор платежа", example = "20", minimum = "1", maximum = "21474836477")
    private int userId;
    @Schema(description = "Иденитификатор предмета", example = "20", minimum = "1", maximum = "21474836477")
    private String itemId;
    @Schema(description = "Скидка", example = "20%", defaultValue = "0.0")
    private double discount;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
