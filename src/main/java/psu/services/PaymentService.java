package psu.services;

import org.springframework.stereotype.Service;
import psu.models.Payment;
import psu.repository.PaymentRepository;

@Service
public class PaymentService {

    private PaymentRepository paymentRepository = PaymentRepository.getInstance();

    public Payment create(Payment newPayment) {
        paymentRepository.create(newPayment);
        return newPayment;
    }

    public Payment get(Integer id) {
        return paymentRepository.get(id);
    }

    public void delete(Integer id) {
        paymentRepository.delete(id);
    }

}
