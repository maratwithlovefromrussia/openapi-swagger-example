package psu.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import psu.models.Payment;
import psu.services.PaymentService;

@Tag(name = "Платеж", description = "Контроллер для управления платежами")
@RestController
@RequestMapping("/payments")
public class PaymentController {

    private final PaymentService paymentService = new PaymentService();

    @Operation(summary = "Метод создания платежа",
            tags = {"Платеж"},
            security = {
                    @SecurityRequirement(name = "Cookie"),
                    @SecurityRequirement(name = "API-KEY")
            },
            responses = {
                    @ApiResponse(description = "CREATED", responseCode = "201",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Payment.class))),
                    @ApiResponse(description = "BAD_REQUEST", responseCode = "400", content = @Content),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401", content = @Content)
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(schema = @Schema(implementation = Payment.class)))
    )
    @PostMapping
    public ResponseEntity<Payment> create(@RequestBody Payment payment) {
        return ResponseEntity.status(201).body(paymentService.create(payment));
    }

    @Operation(summary = "Метод получения платежа по идентификатору платежа",
            tags = {"Платеж"},
            security = {
                    @SecurityRequirement(name = "cookie")
            },
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200",
                            headers = @Header(name = "content-type", description = "тип контента"),
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Payment.class))),
                    @ApiResponse(description = "BAD_REQUEST", responseCode = "400", content = @Content),
                    @ApiResponse(description = "UNAUTHORIZED", responseCode = "401", content = @Content)
            },
            parameters = {@Parameter(name = "id", example = "1"), @Parameter(name = "name", example = "Sasha")}
    )
    @GetMapping("/{id}")
    public Payment get(@PathVariable Integer id) {
        return paymentService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        paymentService.delete(id);
    }

}
