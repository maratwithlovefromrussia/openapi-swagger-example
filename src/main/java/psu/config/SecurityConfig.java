package psu.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityConfig {
    @Value("${auth.secret-cookie}")
    private String secretCookie;
    @Value("${auth.secret-api-key}")
    private String secretApiKey;

    @Bean
    public FilterRegistrationBean<AuthFilter> authFilterFilterRegistrationBean() {
        FilterRegistrationBean<AuthFilter> filterRegistrationBean = new FilterRegistrationBean<>(new AuthFilter(secretCookie, secretApiKey));
        filterRegistrationBean.addUrlPatterns("/payments");
        return filterRegistrationBean;
    }
}
