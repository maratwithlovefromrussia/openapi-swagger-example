package psu.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        servers = @Server(description = "Учебный"),
        info = @Info(
                title = "У вас спина белая",
                description = "Xa-xa-xa",
                license = @License(name = "Лицензия"),
                contact = @Contact(name = "Кто Я?", url = "https://t.me/ThisLinkOpeansAChatWithU", email = "popcorn@hub.ru")),
        security = {@SecurityRequirement(name = "Cookie"), @SecurityRequirement(name = "API-KEY")}
)
@SecurityScheme(
        name = "Cookie",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.COOKIE,
        paramName = "Set-Cookie",
        description = "Авторизация по Cookie, введи секретный ключ!"
)
@SecurityScheme(
        name = "API-KEY",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER,
        paramName = "Api-Key",
        description = "Авторизация по Api-Key, введи секретный ключ!"
)

public class OpenApiConfig {
}
