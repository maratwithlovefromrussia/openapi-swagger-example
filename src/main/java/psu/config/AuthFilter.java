package psu.config;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class AuthFilter implements Filter {
    private final String cookieValue;
    private final String apikeyValue;

    public AuthFilter(String cookieValue, String apikeyValue) {
        this.cookieValue = cookieValue;
        this.apikeyValue = apikeyValue;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String key;
        if (request.getCookies() != null ) {
            key = Arrays.stream(request.getCookies())
                    .filter(cookie -> cookie.getValue().equals(cookieValue))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse("");

        } else {
            key = request.getHeader("Api-Key");
        }

        if (!cookieValue.equals(key) && !apikeyValue.equals(key)) {
            response.sendError(401, "Не пущу");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
